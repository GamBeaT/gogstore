<?php namespace App\Tests\Domain\Model;

use App\Domain\Model\Cart;
use App\Domain\Model\CartItem;
use App\Domain\Model\Product;
use App\Domain\ValueObject\Id;
use App\Domain\ValueObject\Money;
use App\Domain\ValueObject\Name;
use PHPUnit\Framework\TestCase;

class CartTest extends TestCase
{

    public function testZeroTotal()
    {

        $cart = new Cart();
        $result = $cart->getTotal();
        $this->assertEquals(0, $result);

    }

    public function testNonZeroTotal()
    {
        $cart = new Cart();
        $product = Product::createFromStorage(
            new Id(1), new Name("Baldur's Gate"), new Money(199)
        );
        $cartItem = new CartItem(3, $product);
        $cart->addCartItem($cartItem);
        $product = Product::createFromStorage(
            new Id(2), new Name("Fallout"), new Money(299)
        );
        $cartItem = new CartItem(1, $product);
        $cart->addCartItem($cartItem);

        $result = $cart->getTotal();
        $this->assertEquals(896,$result);
    }

    public function testProductsQuantity()
    {
        $cart = new Cart();
        $product = Product::createFromStorage(
            new Id(1), new Name("Baldur's Gate"), new Money(199)
        );
        $cartItem = new CartItem(3, $product);
        $cart->addCartItem($cartItem);
        $product = Product::createFromStorage(
            new Id(2), new Name("Fallout"), new Money(299)
        );
        $cartItem = new CartItem(1, $product);
        $cart->addCartItem($cartItem);

        $result = $cart->getProductsQuantity();
        $this->assertEquals(2, $result);
    }

    public function testMaxItemQuantity()
    {
        $cart = new Cart();
        $product = Product::createFromStorage(
            new Id(1), new Name("Baldur's Gate"), new Money(199)
        );
        $cartItem = new CartItem(3, $product);
        $cart->addCartItem($cartItem);
        $product = Product::createFromStorage(
            new Id(2), new Name("Fallout"), new Money(299)
        );
        $cartItem = new CartItem(1, $product);
        $cart->addCartItem($cartItem);

        $result = $cart->getMaxItemQuantity();
        $this->assertEquals(3, $result);
    }

}