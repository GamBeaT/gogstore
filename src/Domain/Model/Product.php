<?php namespace App\Domain\Model;

use App\Domain\ValueObject\Id;
use App\Domain\ValueObject\Money;
use App\Domain\ValueObject\Name;
use JsonSerializable;

final class Product implements JsonSerializable
{

    protected $id;
    protected $name;
    protected $price;

    private function __construct(Id $id = null, Name $name, Money $price)
    {
        $this->id = $id;
        $this->name = $name;
        $this->price = $price;
    }

    public static function createFromApi(Name $name, Money $price)
    {
        return new self(null, $name, $price);
    }

    public static function createFromStorage(Id $id, Name $name, Money $price)
    {
        return new self($id, $name, $price);
    }

    public function getId()
    {
        if (!$this->id) {
            return null;
        }
        return $this->id->getId();
    }

    public function getName()
    {
        return $this->name->getName();
    }

    public function setName(Name $name)
    {
        $this->name = $name;
    }

    public function getPrice()
    {
        return (int)$this->price->getAmount();
    }

    public function setPrice(Money $price)
    {
        $this->price = $price;
    }

    public function jsonSerialize()
    {
        return [
            'id' => $this->getId(),
            'name' => $this->getName(),
            'price' => $this->getPrice()
        ];
    }
}