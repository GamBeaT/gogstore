<?php namespace App\Domain\Model;

use App\Domain\Exceptions\Catalog\DuplicateProductException;
use JsonSerializable;

final class Catalog implements JsonSerializable
{
    protected const PER_PAGE = 3;
    protected $products = [];

    public function addProduct(Product $product)
    {
        if (!in_array($product->getId(), array_keys($this->products))) {
            $this->products[$product->getId()] = $product;
        } else {
            throw new DuplicateProductException('Catalog cannot have duplicate products');
        }
    }

    public function jsonSerialize()
    {
        $productsCount = count($this->products);
        if ($productsCount <= 3) {
            $products = $this->products;
        } else {
            $products = [];
            $pageCount = 0;
            $productsPerPageCount = 0;
            foreach ($this->products as $product) {
                $products[$pageCount][] = $product;
                $productsPerPageCount++;
                if ((($productsPerPageCount) % self::PER_PAGE) == 0) {
                    $pageCount++;
                }
            }
        }
        return ['products' => $products];
    }
}
