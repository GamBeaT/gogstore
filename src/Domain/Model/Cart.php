<?php namespace App\Domain\Model;

use App\Domain\Model\CartItem;
use ArrayIterator;
use IteratorAggregate;
use JsonSerializable;

class Cart implements JsonSerializable, IteratorAggregate
{

    protected $items = [];

    public function addCartItem(CartItem $cartItem)
    {
        $check = false;
        $key = null;
        foreach ($this->items as $key => $item) {
            if ($cartItem->getProductId() == $item->getProductId()) {
                $this->items[$key]->addQuantity();
                $check = true;
            }
        }
        if (!$check) {
            $this->items[] = $cartItem;
        }

    }

    public function removeCartItem(int $productId)
    {
        foreach ($this->items as $key => $item) {
            if ($productId == $item->getProductId()) {
                unset($this->items[$key]);
            }
        }
    }

    public function getProductsQuantity()
    {
        return sizeof($this->items);
    }

    public function getMaxItemQuantity()
    {
        $max = 0;
        if (sizeof($this->items)) {
            foreach ($this->items as $item) {
                if ($item->getQuantity() > $max) {
                    $max = $item->getQuantity();
                }
            }
        }
        return $max;
    }

    public function getTotal()
    {
        $total = 0;
        if (sizeof($this->items)) {
            foreach ($this->items as $item) {
                $total = $total + ($item->getQuantity() * $item->getProductPrice());
            }
        }
        return $total;
    }

    public function jsonSerialize()
    {
        $json['items'] = [];
        if (sizeof($this->items)) {
            foreach ($this->items as $item) {
                $json['items'][] = $item;
            }
        }
        $json['total'] = $this->getTotal();
        return $json;
    }

    public function getIterator()
    {
        return new ArrayIterator($this->items);
    }
}
