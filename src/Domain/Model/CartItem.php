<?php namespace App\Domain\Model;

use App\Domain\Model\Product;
use JsonSerializable;

class CartItem implements JsonSerializable
{

    protected $quantity;
    protected $product;

    public function __construct(int $quantity = 1, Product $product)
    {
        $this->quantity = $quantity;
        $this->product = $product;
    }

    public function getQuantity()
    {
        return $this->quantity;
    }

    public function addQuantity()
    {
        $this->quantity++;
    }

    public function getProductPrice()
    {
        return $this->product->getPrice();
    }

    public function getProductName()
    {
        return $this->product->getName();
    }

    public function getProductId()
    {
        return $this->product->getId();
    }

    public function getTotal()
    {
        return $this->quantity * $this->getProductPrice();
    }

    public function jsonSerialize()
    {
        return [
            'product_id' => $this->getProductId(),
            'product_name' => $this->getProductName(),
            'product_price' => $this->getProductPrice(),
            'product_quantity' => $this->getQuantity(),
            'product_total' => $this->getTotal()
        ];
    }
}
