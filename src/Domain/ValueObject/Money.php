<?php namespace App\Domain\ValueObject;

use App\Domain\Exceptions\ValueObject\InvalidArgumentException;

class Money
{

    protected $amount;
    protected $currency;

    public function __construct(string $amount, string $currency = 'USD')
    {
        if (!is_numeric($amount) && (!is_integer((int)$amount))) {
            throw new InvalidArgumentException('Money amount is not a valid integer');
        }
        $this->amount = $amount;
        $this->currency = $currency;
    }

    public function getAmount()
    {
        return $this->amount;
    }

    public function getCurrency()
    {
        return $this->currency;
    }

}