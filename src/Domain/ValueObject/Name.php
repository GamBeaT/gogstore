<?php namespace App\Domain\ValueObject;

use App\Domain\Exceptions\ValueObject\InvalidArgumentException;

class Name
{

    protected $name;

    public function __construct(string $name)
    {
        if (!is_string($name)) {
            throw new InvalidArgumentException('Passed name is not a valid string');
        }
        $this->name = $name;
    }

    public function getName()
    {
        return $this->name;
    }

}
