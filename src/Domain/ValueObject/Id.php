<?php namespace App\Domain\ValueObject;

use App\Domain\Exceptions\ValueObject\InvalidArgumentException;

class Id
{

    protected $id;

    public function __construct(int $id)
    {
        if (!is_int($id) || $id <= 0) {
            throw new InvalidArgumentException('Passed ID is not a valid integer');
        }
        $this->id = $id;
    }

    public function getId()
    {
        return $this->id;
    }

}