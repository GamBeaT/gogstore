<?php namespace App\Domain\Commands\Product\UpdateProduct;

use App\Domain\Exceptions\Product\UniqueProductNameException;
use App\Domain\Specification\Product\Specification\UniqueNameSpecification;
use App\Domain\ValueObject\Money;
use App\Domain\ValueObject\Name;
use App\Infrastructure\Framework\Repository\ProductRepository;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

class UpdateProductHandler implements MessageHandlerInterface
{

    protected $productRepository;

    public function __construct(ProductRepository $productRepository)
    {
        $this->productRepository = $productRepository;
    }

    public function handle(UpdateProductCommand $command)
    {
        $product = $this->productRepository->getById($command->getId());
        $product->setName(new Name($command->getName()));
        $product->setPrice(new Money($command->getPrice()));
        $specification = new UniqueNameSpecification($this->productRepository);
        if (!$specification->isSatisfiedBy($product)) {
            throw new UniqueProductNameException("Product with '" . $product->getName() ."' name already exists");
        }
        $this->productRepository->persist($product);
    }

    public function __invoke(UpdateProductCommand $command)
    {
        $this->handle($command);
    }

}
