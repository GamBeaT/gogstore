<?php namespace App\Domain\Commands\Product\UpdateProduct;

use App\Domain\ValueObject\Money;
use App\Infrastructure\Framework\Repository\ProductRepository;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

class UpdateProductPriceHandler implements MessageHandlerInterface
{

    protected $productRepository;

    public function __construct(ProductRepository $productRepository)
    {
        $this->productRepository = $productRepository;
    }

    public function handle(UpdateProductPriceCommand $command)
    {
        $product = $this->productRepository->getById($command->getId());
        $product->setPrice(new Money($command->getPrice()));
        $this->productRepository->persist($product);
    }

    public function __invoke(UpdateProductPriceCommand $command)
    {
        $this->handle($command);
    }

}