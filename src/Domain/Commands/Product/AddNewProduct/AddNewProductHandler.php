<?php namespace App\Domain\Commands\Product\AddNewProduct;

use App\Domain\Events\Product\ProductWasAdded;
use App\Domain\Exceptions\Product\UniqueProductNameException;
use App\Domain\Model\Product;
use App\Domain\Specification\Product\Specification\UniqueNameSpecification;
use App\Domain\ValueObject\Money;
use App\Domain\ValueObject\Name;
use App\Infrastructure\Framework\Repository\ProductRepository;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;
use Symfony\Component\Messenger\MessageBusInterface;

class AddNewProductHandler implements MessageHandlerInterface
{

    protected $eventBus;
    protected $productRepository;

    public function __construct(MessageBusInterface $eventBus, ProductRepository $productRepository)
    {
        $this->eventBus = $eventBus;
        $this->productRepository = $productRepository;
    }

    public function handle(AddNewProductCommand $command)
    {
        $product = Product::createFromApi(new Name($command->getName()), new Money($command->getPrice()));
        $specification = new UniqueNameSpecification($this->productRepository);
        if (!$specification->isSatisfiedBy($product)) {
            throw new UniqueProductNameException("Product with '" . $product->getName() ."' name already exists");
        }
        $product = $this->productRepository->persist($product);
        $this->eventBus->dispatch(new ProductWasAdded($product->getId()));
    }

    public function __invoke(AddNewProductCommand $command)
    {
        $this->handle($command);
    }

}
