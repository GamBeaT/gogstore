<?php namespace App\Domain\Commands\Product\RemoveProduct;

class RemoveProductCommand
{

    protected $id;

    public function __construct(int $id)
    {
        $this->id = $id;
    }

    public function getId()
    {
        return $this->id;
    }

}
