<?php namespace App\Domain\Commands\Product\RemoveProduct;

use App\Domain\Events\Product\ProductWasRemoved;
use App\Infrastructure\Framework\Repository\ProductRepository;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;
use Symfony\Component\Messenger\MessageBusInterface;

class RemoveProductHandler implements MessageHandlerInterface
{

    protected $eventBus;
    protected $productRepository;

    public function __construct(MessageBusInterface $eventBus, ProductRepository $productRepository)
    {
        $this->eventBus = $eventBus;
        $this->productRepository = $productRepository;
    }

    public function handle(RemoveProductCommand $command)
    {
        $product = $this->productRepository->getById($command->getId());
        $this->productRepository->delete($product);
        $this->eventBus->dispatch(new ProductWasRemoved($product->getName(), $product->getPrice()));
    }

    public function __invoke(RemoveProductCommand $command)
    {
        $this->handle($command);
    }

}