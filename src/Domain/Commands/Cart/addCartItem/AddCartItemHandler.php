<?php namespace App\Domain\Commands\Cart\addCartItem;

use App\Domain\Exceptions\Cart\CartRulesException;
use App\Domain\Model\CartItem;
use App\Domain\Specification\Cart\Specification\ProductsLimitSpecification;
use App\Domain\Specification\Cart\Specification\QuantityPerProductLimitSpecification;
use App\Infrastructure\Framework\Repository\CartRepository;
use App\Infrastructure\Framework\Repository\ProductRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

class AddCartItemHandler implements MessageHandlerInterface
{
    protected $cartRepository;
    protected $productRepository;

    public function __construct(CartRepository $cartRepository, ProductRepository $productRepository)
    {
        $this->cartRepository = $cartRepository;
        $this->productRepository = $productRepository;
    }

    public function handle(AddCartItemCommand $command)
    {
        $cart = $this->cartRepository->getCartForUser($command->getUserId());
        $product = $this->productRepository->getById($command->getProductId());
        $cartItem = new CartItem(1, $product);
        $cart->addCartItem($cartItem);
        $specification = new ProductsLimitSpecification();
        if (!$specification->isSatisfiedBy($cart)) {
            throw new CartRulesException('Cart contains too many different products');
        }
        $specification = new QuantityPerProductLimitSpecification();
        if (!$specification->isSatisfiedBy($cart)) {
            throw new CartRulesException('One or more products exceed quantity limit');
        }
        $this->cartRepository->save($cart, $command->getUserId());
    }

    public function __invoke(AddCartItemCommand $command)
    {
        $this->handle($command);
    }

}
