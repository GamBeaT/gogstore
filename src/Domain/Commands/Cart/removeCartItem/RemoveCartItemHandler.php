<?php namespace App\Domain\Commands\Cart\removeCartItem;

use App\Domain\Events\Cart\ProductRemovedFromCart;
use App\Domain\Exceptions\Cart\CartRulesException;
use App\Domain\Specification\Cart\Specification\ProductsLimitSpecification;
use App\Domain\Specification\Cart\Specification\QuantityPerProductLimitSpecification;
use App\Infrastructure\Framework\Repository\CartRepository;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;
use Symfony\Component\Messenger\MessageBusInterface;

class RemoveCartItemHandler implements MessageHandlerInterface
{

    protected $eventBus;
    protected $cartRepository;

    public function __construct(MessageBusInterface $eventBus, CartRepository $cartRepository)
    {
        $this->eventBus = $eventBus;
        $this->cartRepository = $cartRepository;
    }

    public function handle(RemoveCartItemCommand $command)
    {
        $cart = $this->cartRepository->getCartForUser($command->getUserId());
        $cart->removeCartItem($command->getProductId());
        $specification = new ProductsLimitSpecification();
        if (!$specification->isSatisfiedBy($cart)) {
            throw new CartRulesException('Cart contains too many different products');
        }
        $specification = new QuantityPerProductLimitSpecification();
        if (!$specification->isSatisfiedBy($cart)) {
            throw new CartRulesException('One or more products exceed quantity limit');
        }
        $this->cartRepository->save($cart, $command->getUserId());
        $this->eventBus->dispatch(new ProductRemovedFromCart($command->getUserId(), $command->getProductId()));
    }

    public function __invoke(RemoveCartItemCommand $command)
    {
        $this->handle($command);
    }

}
