<?php namespace App\Domain\Commands\Cart\removeCartItem;

class RemoveCartItemCommand
{

    protected $userId;
    protected $productId;

    public function __construct(int $userId, int $productId)
    {
        $this->userId = $userId;
        $this->productId = $productId;
    }

    public function getUserId()
    {
        return $this->userId;
    }

    public function getProductId()
    {
        return $this->productId;
    }

}
