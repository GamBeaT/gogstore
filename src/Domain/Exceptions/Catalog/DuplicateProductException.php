<?php namespace App\Domain\Exceptions\Catalog;

use App\Domain\Exceptions\DomainException;

class DuplicateProductException extends DomainException
{

}
