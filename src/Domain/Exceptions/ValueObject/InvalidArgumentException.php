<?php namespace App\Domain\Exceptions\ValueObject;

use App\Domain\Exceptions\DomainException;

class InvalidArgumentException extends DomainException
{

}
