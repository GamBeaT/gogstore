<?php namespace App\Domain\Exceptions\Product;

use App\Domain\Exceptions\DomainException;

class ProductNotFoundException extends DomainException
{

}
