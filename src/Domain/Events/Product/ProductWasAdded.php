<?php namespace App\Domain\Events\Product;

class ProductWasAdded
{

    protected $id;

    public function __construct(int $id)
    {
        $this->id = $id;
    }

    public function getId()
    {
        return $this->id;
    }

}
