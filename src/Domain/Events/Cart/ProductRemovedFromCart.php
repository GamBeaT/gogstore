<?php namespace App\Domain\Events\Cart;

class ProductRemovedFromCart
{

    protected $userId;
    protected $productId;

    public function __construct(int $userId, int $productId)
    {
        $this->userId = $userId;
        $this->productId = $productId;
    }

    public function getUserId()
    {
        return $this->userId;
    }

    public function getProductId()
    {
        return $this->productId;
    }

}