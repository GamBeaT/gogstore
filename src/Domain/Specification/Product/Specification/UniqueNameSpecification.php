<?php namespace App\Domain\Specification\Product\Specification;

use App\Domain\Model\Product;
use App\Domain\Specification\Product\Specification;
use App\Infrastructure\Framework\Repository\ProductRepository;

class UniqueNameSpecification implements Specification
{

    protected $productRepository;

    public function __construct(ProductRepository $productRepository)
    {
        $this->productRepository = $productRepository;
    }

    public function isSatisfiedBy(Product $product)
    {
        return $this->productRepository->checkUniqueName($product);
    }
}