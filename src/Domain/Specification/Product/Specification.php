<?php namespace App\Domain\Specification\Product;

use App\Domain\Model\Product;

interface Specification
{

    public function isSatisfiedBy(Product $product);

}