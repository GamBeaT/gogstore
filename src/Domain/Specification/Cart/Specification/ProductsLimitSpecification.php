<?php namespace App\Domain\Specification\Cart\Specification;

use App\Domain\Model\Cart;
use App\Domain\Specification\Cart\Specification;

class ProductsLimitSpecification implements Specification
{

    public function isSatisfiedBy(Cart $cart)
    {
        if ($cart->getProductsQuantity() > 3) {
            return false;
        }
        return true;
    }
}
