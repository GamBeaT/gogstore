<?php namespace App\Domain\Specification\Cart\Specification;

use App\Domain\Model\Cart;
use App\Domain\Specification\Cart\Specification;

class QuantityPerProductLimitSpecification implements Specification
{

    public function isSatisfiedBy(Cart $cart)
    {
        if ($cart->getMaxItemQuantity() > 10) {
            return false;
        }
        return true;
    }
}
