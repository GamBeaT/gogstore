<?php namespace App\Domain\Specification\Cart;

use App\Domain\Model\Cart;

interface Specification
{

    public function isSatisfiedBy(Cart $cart);

}
