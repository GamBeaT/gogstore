<?php namespace App\Domain\Repository;

use App\Domain\Model\Product as Domain;
use App\Domain\ValueObject\Id;

interface ProductRepository
{

    public function getAll();
    public function getById(int $id);
    public function persist(Domain $product);
    public function checkUniqueName(Domain $product);
    public function delete(Domain $product);

}
