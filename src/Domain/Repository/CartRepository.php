<?php namespace App\Domain\Repository;

use App\Domain\Model\Cart;

interface CartRepository
{

    public function getCartForUser(int $userId);
    public function save(Cart $cart, int $userId);

}