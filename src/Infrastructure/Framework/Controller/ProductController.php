<?php namespace App\Infrastructure\Framework\Controller;

use App\Domain\Commands\Product\AddNewProduct\AddNewProductCommand;
use App\Domain\Commands\Product\RemoveProduct\RemoveProductCommand;
use App\Domain\Commands\Product\UpdateProduct\UpdateProductCommand;
use App\Domain\Commands\Product\UpdateProduct\UpdateProductNameCommand;
use App\Domain\Commands\Product\UpdateProduct\UpdateProductPriceCommand;
use App\Domain\Exceptions\DomainException;
use App\Infrastructure\Framework\Repository\ProductRepository;
use App\Products\Domain\Model\Product;
use App\Service\JsonRequest\Handler;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Messenger\Exception\HandlerFailedException;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Routing\Annotation\Route;

class ProductController extends AbstractController
{

    /**
     * @Route("/api/products", name="products", methods={"GET"})
     */
    public function getAll(ProductRepository $repository)
    {
        $catalog = $repository->getAll();
        return new JsonResponse($catalog, Response::HTTP_OK);
    }


    /**
     * @Route("/api/product", name="product", methods={"POST"})
     */

    public function addProduct(Request $request, MessageBusInterface $commandBus)
    {
        $request = Handler::handle($request);
        $name = $request->request->get('name');
        $price = $request->request->get('price');

        if ($name && $price) {
            try {
                $commandBus->dispatch(new AddNewProductCommand($name, $price));
                return new JsonResponse(['message' => 'New product added'], Response::HTTP_OK);
            } catch (HandlerFailedException $ex) {
                return new JsonResponse(['error' => $ex->getMessage()], Response::HTTP_BAD_REQUEST);
            }
        }
        return new JsonResponse(['error' => 'Invalid arguments'], Response::HTTP_BAD_REQUEST);
    }

    /**
     * @Route("/api/product/remove/{id}", name="remove_product", methods={"DELETE"})
     */

    public function removeProduct (MessageBusInterface $commandBus, $id)
    {
        if ($id and is_numeric($id)) {
            try {
                $commandBus->dispatch(new RemoveProductCommand($id));
                return new JsonResponse(['message' => 'Product removed'], Response::HTTP_OK);
            } catch (HandlerFailedException $ex) {
                return new JsonResponse(['error' => $ex->getMessage()], Response::HTTP_BAD_REQUEST);
            }
        }
        return new JsonResponse(['error' => 'Invalid arguments'], Response::HTTP_BAD_REQUEST);
    }

    /**
     * @Route("/api/product/update/{id}", name="update_product", methods={"PUT"})
     */

    public function updateProduct(Request $request, MessageBusInterface $commandBus, $id)
    {
        if ($id and is_numeric($id)) {
            $request = Handler::handle($request);
            $name = $request->request->get('name');
            $price = $request->request->get('price');
            if ($name && $price) {
                try {
                    $commandBus->dispatch(new UpdateProductCommand($id, $name, $price));
                    return new JsonResponse(['message' => 'Product has been updated'], Response::HTTP_OK);
                } catch (HandlerFailedException $ex) {
                    return new JsonResponse(['error' => $ex->getMessage()], Response::HTTP_BAD_REQUEST);
                }
            }
        }
        return new JsonResponse(['error' => 'Invalid arguments'], Response::HTTP_BAD_REQUEST);
    }

    /**
     * @Route("/api/product/update/{id}", name="update_product_partial", methods={"PATCH"})
     */

    public function updateProductPartial(Request $request, MessageBusInterface $commandBus, $id)
    {
        if ($id and is_numeric($id)) {
            $request = Handler::handle($request);
            $name = $request->request->get('name');
            $price = $request->request->get('price');
            if ($name && $price) {
                return new JsonResponse(['error' => 'PATCH endpoint is used only to update resource partially'], Response::HTTP_BAD_REQUEST);
            }
            if ($name) {
                $commandBus->dispatch(new UpdateProductNameCommand($id, $name));
                return new JsonResponse(['message' => 'Product name has been updated'], Response::HTTP_OK);
            }
            if ($price) {
                $commandBus->dispatch(new UpdateProductPriceCommand($id, $price));
                return new JsonResponse(['message' => 'Product price has been updated'], Response::HTTP_OK);
            }
        }
        return new JsonResponse(['error' => 'Invalid arguments'], Response::HTTP_BAD_REQUEST);
    }

}
