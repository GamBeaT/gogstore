<?php namespace App\Infrastructure\Framework\Controller;

use App\Domain\Commands\Cart\addCartItem\AddCartItemCommand;
use App\Domain\Commands\Cart\removeCartItem\RemoveCartItemCommand;
use App\Infrastructure\Framework\Repository\CartRepository;
use App\Service\JsonRequest\Handler;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Messenger\Exception\HandlerFailedException;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Routing\Annotation\Route;

class CartController extends AbstractController
{

    /**
     * @Route("/api/cart/{id}", name="cart", methods={"GET"})
     */

    public function getCart(int $id, CartRepository $cartRepository)
    {
        if (!$id || !is_numeric($id)) {
            return new JsonResponse(['error' => 'Invalid arguments'], Response::HTTP_BAD_REQUEST);
        }
        $cart = $cartRepository->getCartForUser($id);
        return new JsonResponse($cart, Response::HTTP_OK);
    }

    /**
     * @Route("/api/cart/{id}", name="cart_add_item", methods={"POST"})
     */

    public function addCartItem(Request $request, int $id, MessageBusInterface $commandBus)
    {
        $request = Handler::handle($request);
        $productId = $request->request->get('product_id');
        if ($productId) {
            try {
                $commandBus->dispatch(new AddCartItemCommand($id, $productId));
                return new JsonResponse(['message' => 'Product added to cart'], Response::HTTP_OK);
            } catch (HandlerFailedException $ex) {
                return new JsonResponse(['error' => $ex->getMessage()], Response::HTTP_BAD_REQUEST);
            }
        }
        return new JsonResponse(['error' => 'Invalid arguments'], Response::HTTP_BAD_REQUEST);
    }

    /**
     * @Route("/api/cart/{id}", name="cart_remove_item", methods={"DELETE"})
     */

    public function removeCartItem(Request $request, int $id, MessageBusInterface $commandBus)
    {
        $request = Handler::handle($request);
        $productId = $request->request->get('product_id');
        if ($productId) {
            try {
                $commandBus->dispatch(new RemoveCartItemCommand($id, $productId));
                return new JsonResponse(['message' => 'Product removed from cart'], Response::HTTP_OK);
            } catch (HandlerFailedException $ex) {
                return new JsonResponse(['error' => $ex->getMessage()], Response::HTTP_BAD_REQUEST);
            }
        }
        return new JsonResponse(['error' => 'Invalid arguments'], Response::HTTP_BAD_REQUEST);
    }

}