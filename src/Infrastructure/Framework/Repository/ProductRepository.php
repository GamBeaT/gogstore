<?php namespace App\Infrastructure\Framework\Repository;

use App\Domain\Exceptions\Product\ProductNotFoundException;
use App\Domain\Model\Catalog;
use App\Domain\Model\Product as Domain;
use App\Domain\ValueObject\Id;
use App\Entity\Product as Entity;
use App\Infrastructure\Doctrine\ProductTransformer;
use App\Domain\Repository\ProductRepository as DomainRepository;
use Doctrine\ORM\EntityManagerInterface;

class ProductRepository implements DomainRepository
{

    protected $entityManager;
    protected $entityRepository;
    protected $productTransformer;

    public function __construct(EntityManagerInterface $entityManager, ProductTransformer $productTransformer)
    {
        $this->entityManager = $entityManager;
        $this->entityRepository = $this->entityManager->getRepository(Entity::class);
        $this->productTransformer = $productTransformer;
    }

    public function getAll()
    {
        $products = $this->entityRepository->findBy([], ['name' => 'asc']);
        $catalog = new Catalog();
        if (sizeof($products)) {
            foreach ($products as $product) {
                $catalog->addProduct($this->productTransformer->entityToDomain($product));
            }
        }
        return $catalog;
    }

    public function getById(int $id)
    {
        $product = $this->entityRepository->find($id);
        if ($product) {
            return $this->productTransformer->entityToDomain($product);
        }
        throw new ProductNotFoundException('Product with ID: ' . $id .' not found');
    }

    public function persist(Domain $product)
    {
        $entity = $this->productTransformer->domainToEntity($product);
        $this->entityManager->persist($entity);
        $this->entityManager->flush();
        return $this->productTransformer->entityToDomain($entity);
    }

    public function checkUniqueName(Domain $product)
    {
        $entity = $this->entityRepository->findOneBy(['name' => $product->getName()]);
        if ($entity) {
            if ($product->getName() == $entity->getName() && $product->getId() != $entity->getId()) {
                return false;
            }
        }
        return true;
    }

    public function delete(Domain $product)
    {
        $entity = $this->productTransformer->domainToEntity($product);
        $this->entityManager->remove($entity);
        $this->entityManager->flush();
    }

}