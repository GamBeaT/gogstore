<?php namespace App\Infrastructure\Framework\Repository;

use App\Domain\Model\Cart;
use App\Domain\Model\CartItem;
use App\Domain\Repository\CartRepository as DomainRepository;
use App\Entity\CartItem as Entity;
use App\Infrastructure\Doctrine\CartTransformer;
use Doctrine\ORM\EntityManagerInterface;

class CartRepository implements DomainRepository
{

    protected $entityManager;
    protected $entityRepository;
    protected $cartTransformer;
    protected $productRepository;

    public function __construct(EntityManagerInterface $entityManager, CartTransformer $cartTransformer, ProductRepository $productRepository)
    {
        $this->entityManager = $entityManager;
        $this->entityRepository = $this->entityManager->getRepository(Entity::class);
        $this->cartTransformer = $cartTransformer;
        $this->productRepository = $productRepository;
    }

    public function getCartForUser(int $userId)
    {
        $cart = $this->entityRepository->findBy(['userId' => $userId]);
        $shoppingCart = new Cart();
        if ($cart) {
            foreach ($cart as $item) {
                $product = $this->productRepository->getById($item->getProductId());
                $cartItem = new CartItem($item->getQuantity(), $product);
                $shoppingCart->addCartItem($cartItem);
            }
        }
        return $shoppingCart;
    }

    public function save(Cart $cart, int $userId)
    {
        $dbCart = $this->entityRepository->findBy(['userId' => $userId]);
        if ($dbCart) {
            foreach ($dbCart as $item) {
                $this->entityManager->remove($item);
            }
            $this->entityManager->flush();
        }
        foreach ($cart as $item) {
            $cartItem = new Entity();
            $cartItem->setUserId($userId);
            $cartItem->setQuantity($item->getQuantity());
            $cartItem->setProductId($item->getProductId());
            $this->entityManager->persist($cartItem);
        }
        $this->entityManager->flush();
    }

}
