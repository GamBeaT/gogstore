<?php namespace App\Infrastructure\Doctrine;

use App\Domain\ValueObject\Id;
use App\Domain\ValueObject\Money;
use App\Domain\ValueObject\Name;
use Doctrine\ORM\EntityManagerInterface;
use App\Entity\Product as Entity;
use App\Domain\Model\Product as Domain;

class ProductTransformer
{

    protected $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function entityToDomain(Entity $entity)
    {
        $domain = Domain::createFromStorage(
            new Id($entity->getId()),
            new Name($entity->getName()),
            new Money($entity->getPrice())
        );
        return $domain;
    }

    public function domainToEntity(Domain $domain)
    {
        if ($domain->getId()) {
            $entity = $this->entityManager->find(Entity::class, $domain->getId());
            $entity->setName($domain->getName());
            $entity->setPrice($domain->getPrice());
        } else {
            $entity = new Entity();
            $entity->setName($domain->getName());
            $entity->setPrice($domain->getPrice());
        }
        return $entity;
    }

}
