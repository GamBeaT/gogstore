<?php namespace App\Service\JsonRequest;

use Symfony\Component\HttpFoundation\Request;

class Handler
{

    public static function handle(Request $request)
    {
        if (0 === strpos($request->headers->get('Content-Type'), 'application/json')) {
            $data = json_decode($request->getContent(), true);
            $request->request->replace(is_array($data) ? $data : array());
        }
        return $request;
    }

}
